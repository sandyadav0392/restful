package com.example.demo.rest;

import javax.websocket.server.PathParam;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.book.Book;
import com.example.demo.dto.BookDTO;
import com.example.demo.service.BookService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;

// create our class as a rest api

@RestController
@Api("this is Rest of Controller Class")
public class MyRestController {
	      @Autowired
	     private BookService servie;
	    public MyRestController() {
        System.out.println("MyRestController.MyRestController()");
	    }
       // to bind our method to http GET request
	  @ApiOperation("this use to get All Book Details") 
	  @GetMapping(value = "/get",produces = "application/json")
	  public @ApiResponse(code = 200, message = "Return Detialis about the book") Book getBook(@PathParam("id")int id) {
		  System.out.println("MyRestController.getBook()");
		  BookDTO dto=null; 
		  Book book=new Book();
		   dto= servie.getBookByService(id);
		    BeanUtils.copyProperties(dto, book);
	    return book;
	  }
	  
	  @PostMapping(value = "/add",consumes = "application/json")
	  @ApiOperation("Show Book Details")

	  public @ApiResponse(code = 200, message = "For Confermation") @ResponseBody String addBook( @RequestBody Book book) {
		   System.out.println("MyRestController.addBook()");  
		  System.out.println(book);
		  return "Book added successfully...";
	  }
	  
}
