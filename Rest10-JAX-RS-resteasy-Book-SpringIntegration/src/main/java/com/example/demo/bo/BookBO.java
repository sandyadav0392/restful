package com.example.demo.bo;


import lombok.Data;

@Data
public class BookBO {
	 private int id;
	  private String name;
	  private String author;
}
