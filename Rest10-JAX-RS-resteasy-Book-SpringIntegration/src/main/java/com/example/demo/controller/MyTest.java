package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyTest {

	@GetMapping(value = "/hello")
	 public String sayHello() {
		  System.out.println("MyTest.sayHello()");
		 return "******** Welcome hello Sandeep *******";
	 }
	
}
