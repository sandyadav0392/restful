package com.example.demo.entity;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "book")
public class BookEntity {
	@Id
	 private int id;
	  private String name;
	  private String author;
}
