package com.example.demo.dto;

import lombok.Data;

@Data
public class BookDTO {
	 private int id;
	  private String name;
	  private String author;
}
