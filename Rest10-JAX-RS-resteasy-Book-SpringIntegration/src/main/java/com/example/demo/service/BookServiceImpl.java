package com.example.demo.service;


import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.BookDAO;
import com.example.demo.dto.BookDTO;
import com.example.demo.entity.BookEntity;

@Service
public class BookServiceImpl implements BookService {
     @Autowired
	 private BookDAO dao;
	
	
	@Override
	public BookDTO getBookByService(int id) {
		 
		     BookEntity findById = dao.findById(id).get();
		             BookDTO dto=new BookDTO();
		           BeanUtils.copyProperties(findById, dto);
		return dto;
	}

}
