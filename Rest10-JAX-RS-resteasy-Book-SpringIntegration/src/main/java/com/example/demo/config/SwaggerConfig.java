package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {

	
// its provide api info 
@Bean
public Docket api() { 
    return new Docket(DocumentationType.SWAGGER_2)  
      .select()                                  
      .apis(RequestHandlerSelectors.basePackage("com.example.demo.rest"))              
      .paths(PathSelectors.any()) 
      .build()
      .apiInfo(apiInfo())
      ;                                           
}

 public ApiInfo apiInfo() {
	 ApiInfo info=new ApiInfo("Spring Boot Rest Api",
			 "This is for Testing",
			  "1.0", 
			  "sandeep.com",
			    "sandeep",
			  "Apache.org",
			     "www.sandy.com");
	 
	 
	 return info;
 }
}